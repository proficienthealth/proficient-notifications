require 'aws-sdk-core'
require 'aws-sigv4'

require_relative 'proficient-notifications/types'
require_relative 'proficient-notifications/client_api'
require_relative 'proficient-notifications/client'
require_relative 'proficient-notifications/errors'
require_relative 'proficient-notifications/resource'
require_relative 'proficient-notifications/customizations'

# This module provides support for Notification Management REST. This module is available in the
# `proficient-notifications` gem.
#
# # Client
#
# The {Client} class provides one method for each API operation. Operation
# methods each accept a hash of request parameters and return a response
# structure.
#
# See {Client} for more information.
#
# # Errors
#
# Errors returned from Notification Management REST all
# extend {Errors::ServiceError}.
#
#     begin
#       # do stuff
#     rescue Notifications::Errors::ServiceError
#       # rescues all service API errors
#     end
#
# See {Errors} for more information.
#
# @service
module Notifications

  GEM_VERSION = '1.0'

end
