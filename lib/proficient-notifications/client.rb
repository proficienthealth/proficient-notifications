require 'seahorse/client/plugins/content_length.rb'
require 'aws-sdk-core/plugins/logging.rb'
require 'aws-sdk-core/plugins/param_converter.rb'
require 'aws-sdk-core/plugins/param_validator.rb'
require 'aws-sdk-core/plugins/helpful_socket_errors.rb'
require 'aws-sdk-core/plugins/retry_errors.rb'
require 'aws-sdk-core/plugins/global_configuration.rb'
require 'aws-sdk-core/plugins/response_paging.rb'
require 'aws-sdk-core/plugins/stub_responses.rb'
require 'aws-sdk-core/plugins/idempotency_token.rb'
require 'aws-sdk-core/plugins/jsonvalue_converter.rb'
require 'aws-sdk-core/plugins/client_metrics_plugin.rb'
require 'aws-sdk-core/plugins/client_metrics_send_plugin.rb'
require 'aws-sdk-core/plugins/protocols/api_gateway.rb'
require 'aws-sdk-core/plugins/api_key.rb'
require 'aws-sdk-core/plugins/apig_user_agent.rb'
require 'aws-sdk-core/plugins/apig_authorizer_token.rb'
require 'aws-sdk-core/plugins/apig_credentials_configuration.rb'
require 'proficient-notifications/plugins/authorizer.rb'
require 'proficient-notifications/plugins/apig_endpoint.rb'

Aws::Plugins::GlobalConfiguration.add_identifier(:notifications)

module Notifications
  class Client < Seahorse::Client::Base

    include Aws::ClientStubs

    @identifier = :notifications

    set_api(ClientApi::API)

    add_plugin(Seahorse::Client::Plugins::ContentLength)
    add_plugin(Aws::Plugins::Logging)
    add_plugin(Aws::Plugins::ParamConverter)
    add_plugin(Aws::Plugins::ParamValidator)
    add_plugin(Aws::Plugins::HelpfulSocketErrors)
    add_plugin(Aws::Plugins::RetryErrors)
    add_plugin(Aws::Plugins::GlobalConfiguration)
    add_plugin(Aws::Plugins::ResponsePaging)
    add_plugin(Aws::Plugins::StubResponses)
    add_plugin(Aws::Plugins::IdempotencyToken)
    add_plugin(Aws::Plugins::JsonvalueConverter)
    add_plugin(Aws::Plugins::ClientMetricsPlugin)
    add_plugin(Aws::Plugins::ClientMetricsSendPlugin)
    add_plugin(Aws::Plugins::Protocols::ApiGateway)
    add_plugin(Aws::Plugins::ApiKey)
    add_plugin(Aws::Plugins::APIGUserAgent)
    add_plugin(Aws::Plugins::APIGAuthorizerToken)
    add_plugin(Aws::Plugins::APIGCredentialsConfiguration)
    add_plugin(Notifications::Plugins::Authorizer)
    add_plugin(Notifications::Plugins::APIGEndpoint)

    # @overload initialize(options)
    #   @param [Hash] options
    #   @option options [String] :access_key_id
    #
    #   @option options [String] :api_key
    #     When provided, `x-api-key` header will be injected with the value provided.
    #
    #   @option options [Boolean] :client_side_monitoring (false)
    #     When `true`, client-side metrics will be collected for all API requests from
    #     this client.
    #
    #   @option options [String] :client_side_monitoring_client_id ("")
    #     Allows you to provide an identifier for this client which will be attached to
    #     all generated client side metrics. Defaults to an empty string.
    #
    #   @option options [Integer] :client_side_monitoring_port (31000)
    #     Required for publishing client metrics. The port that the client side monitoring
    #     agent is running on, where client metrics will be published via UDP.
    #
    #   @option options [Aws::ClientSideMonitoring::Publisher] :client_side_monitoring_publisher (Aws::ClientSideMonitoring::Publisher)
    #     Allows you to provide a custom client-side monitoring publisher class. By default,
    #     will use the Client Side Monitoring Agent Publisher.
    #
    #   @option options [Boolean] :convert_params (true)
    #     When `true`, an attempt is made to coerce request parameters into
    #     the required types.
    #
    #   @option options [Aws::CredentialProvider] :credentials
    #     AWS Credentials options is only required when your API uses
    #     [AWS Signature Version 4](http://docs.aws.amazon.com/general/latest/gr/signature-version-4.html),
    #     more AWS Credentials Configuration Options are available [here](https://github.com/aws/aws-sdk-ruby#configuration).
    #
    #   @option options [Aws::Log::Formatter] :log_formatter (Aws::Log::Formatter.default)
    #     The log formatter.
    #
    #   @option options [Symbol] :log_level (:info)
    #     The log level to send messages to the `:logger` at.
    #
    #   @option options [Logger] :logger
    #     The Logger instance to send log messages to.  If this option
    #     is not set, logging will be disabled.
    #
    #   @option options [String] :profile
    #
    #   @option options [Float] :retry_base_delay (0.3)
    #     The base delay in seconds used by the default backoff function.
    #
    #   @option options [Symbol] :retry_jitter (:none)
    #     A delay randomiser function used by the default backoff function. Some predefined functions can be referenced by name - :none, :equal, :full, otherwise a Proc that takes and returns a number.
    #
    #     @see https://www.awsarchitectureblog.com/2015/03/backoff.html
    #
    #   @option options [Integer] :retry_limit (3)
    #     The maximum number of times to retry failed requests.  Only
    #     ~ 500 level server errors and certain ~ 400 level client errors
    #     are retried.  Generally, these are throttling errors, data
    #     checksum errors, networking errors, timeout errors and auth
    #     errors from expired credentials.
    #
    #   @option options [Integer] :retry_max_delay (0)
    #     The maximum number of seconds to delay between retries (0 for no limit) used by the default backoff function.
    #
    #   @option options [String] :secret_access_key
    #
    #   @option options [String] :session_token
    #
    #   @option options [Boolean] :stub_responses (false)
    #     Causes the client to return stubbed responses. By default
    #     fake responses are generated and returned. You can specify
    #     the response data to return or errors to raise by calling
    #     {ClientStubs#stub_responses}. See {ClientStubs} for more information.
    #
    #     ** Please note ** When response stubbing is enabled, no HTTP
    #     requests are made, and retries are disabled.
    #
    #   @option options [Boolean] :validate_params (true)
    #     When `true`, request parameters are validated before
    #     sending the request.
    #
    #   @option options [URI::HTTP,String] :http_proxy A proxy to send
    #     requests through.  Formatted like 'http://proxy.com:123'.
    #
    #   @option options [Float] :http_open_timeout (15) The number of
    #     seconds to wait when opening a HTTP session before rasing a
    #     `Timeout::Error`.
    #
    #   @option options [Integer] :http_read_timeout (60) The default
    #     number of seconds to wait for response data.  This value can
    #     safely be set
    #     per-request on the session yeidled by {#session_for}.
    #
    #   @option options [Float] :http_idle_timeout (5) The number of
    #     seconds a connection is allowed to sit idble before it is
    #     considered stale.  Stale connections are closed and removed
    #     from the pool before making a request.
    #
    #   @option options [Float] :http_continue_timeout (1) The number of
    #     seconds to wait for a 100-continue response before sending the
    #     request body.  This option has no effect unless the request has
    #     "Expect" header set to "100-continue".  Defaults to `nil` which
    #     disables this behaviour.  This value can safely be set per
    #     request on the session yeidled by {#session_for}.
    #
    #   @option options [Boolean] :http_wire_trace (false) When `true`,
    #     HTTP debug output will be sent to the `:logger`.
    #
    #   @option options [Boolean] :ssl_verify_peer (true) When `true`,
    #     SSL peer certificates are verified when establishing a
    #     connection.
    #
    #   @option options [String] :ssl_ca_bundle Full path to the SSL
    #     certificate authority bundle file that should be used when
    #     verifying peer certificates.  If you do not pass
    #     `:ssl_ca_bundle` or `:ssl_ca_directory` the the system default
    #     will be used if available.
    #
    #   @option options [String] :ssl_ca_directory Full path of the
    #     directory that contains the unbundled SSL certificate
    #     authority files for verifying peer certificates.  If you do
    #     not pass `:ssl_ca_bundle` or `:ssl_ca_directory` the the
    #     system default will be used if available.
    #
    def initialize(*args)
      super
    end

    # @!group API Operations

    # @option params [required, Types::NotificationSystemResource] :notification_system_resource
    #
    # @return [Types::createNotificationSystemUsingPOSTResponse] Returns a {Seahorse::Client::Response response} object which responds to the following methods:
    #
    #   * {Types::createNotificationSystemUsingPOSTResponse#notification_system_resource #notification_system_resource} => Types::NotificationSystemResource
    #
    # @example Request syntax with placeholder values
    #
    #   resp = client.create_notification_system_using_post({
    #     notification_system_resource: { # required
    #       create_time: Time.now,
    #       description: "__string",
    #       id: "__string",
    #       last_modified_time: Time.now,
    #       name: "__string",
    #       reference_id: "__string",
    #     },
    #   })
    #
    # @example Response structure
    #
    #   resp.notification_system_resource.create_time #=> Time
    #   resp.notification_system_resource.description #=> String
    #   resp.notification_system_resource.id #=> String
    #   resp.notification_system_resource.last_modified_time #=> Time
    #   resp.notification_system_resource.name #=> String
    #   resp.notification_system_resource.reference_id #=> String
    #
    # @overload create_notification_system_using_post(params = {})
    # @param [Hash] params ({})
    def create_notification_system_using_post(params = {}, options = {})
      req = build_request(:create_notification_system_using_post, params)
      req.send_request(options)
    end

    # @option params [required, String] :system_id
    #
    # @return [Struct] Returns an empty {Seahorse::Client::Response response}.
    #
    # @example Request syntax with placeholder values
    #
    #   resp = client.delete_notification_system_using_delete({
    #     system_id: "__string", # required
    #   })
    #
    # @overload delete_notification_system_using_delete(params = {})
    # @param [Hash] params ({})
    def delete_notification_system_using_delete(params = {}, options = {})
      req = build_request(:delete_notification_system_using_delete, params)
      req.send_request(options)
    end

    # @option params [required, String] :system_id
    #
    # @return [Types::getNotificationSystemUsingGETResponse] Returns a {Seahorse::Client::Response response} object which responds to the following methods:
    #
    #   * {Types::getNotificationSystemUsingGETResponse#notification_system_resource #notification_system_resource} => Types::NotificationSystemResource
    #
    # @example Request syntax with placeholder values
    #
    #   resp = client.get_notification_system_using_get({
    #     system_id: "__string", # required
    #   })
    #
    # @example Response structure
    #
    #   resp.notification_system_resource.create_time #=> Time
    #   resp.notification_system_resource.description #=> String
    #   resp.notification_system_resource.id #=> String
    #   resp.notification_system_resource.last_modified_time #=> Time
    #   resp.notification_system_resource.name #=> String
    #   resp.notification_system_resource.reference_id #=> String
    #
    # @overload get_notification_system_using_get(params = {})
    # @param [Hash] params ({})
    def get_notification_system_using_get(params = {}, options = {})
      req = build_request(:get_notification_system_using_get, params)
      req.send_request(options)
    end

    # @option params [String] :offset
    #
    # @option params [String] :page_number
    #
    # @option params [String] :page_size
    #
    # @option params [String] :paged
    #
    # @option params [String] :search_term
    #
    # @option params [String] :sort_sorted
    #
    # @option params [String] :sort_unsorted
    #
    # @option params [String] :unpaged
    #
    # @return [Types::searchNotificationSystemsUsingGETResponse] Returns a {Seahorse::Client::Response response} object which responds to the following methods:
    #
    #   * {Types::searchNotificationSystemsUsingGETResponse#array_of_notification_system_resource #array_of_notification_system_resource} => Array&lt;Types::NotificationSystemResource&gt;
    #
    # @example Request syntax with placeholder values
    #
    #   resp = client.search_notification_systems_using_get({
    #     offset: "__string",
    #     page_number: "__string",
    #     page_size: "__string",
    #     paged: "__string",
    #     search_term: "__string",
    #     sort_sorted: "__string",
    #     sort_unsorted: "__string",
    #     unpaged: "__string",
    #   })
    #
    # @example Response structure
    #
    #   resp.array_of_notification_system_resource #=> Array
    #   resp.array_of_notification_system_resource[0].create_time #=> Time
    #   resp.array_of_notification_system_resource[0].description #=> String
    #   resp.array_of_notification_system_resource[0].id #=> String
    #   resp.array_of_notification_system_resource[0].last_modified_time #=> Time
    #   resp.array_of_notification_system_resource[0].name #=> String
    #   resp.array_of_notification_system_resource[0].reference_id #=> String
    #
    # @overload search_notification_systems_using_get(params = {})
    # @param [Hash] params ({})
    def search_notification_systems_using_get(params = {}, options = {})
      req = build_request(:search_notification_systems_using_get, params)
      req.send_request(options)
    end

    # @option params [required, Types::EmailNotificationResource] :email_notification_resource
    #
    # @option params [required, String] :system_id
    #
    # @return [Types::sendEmailNotificationUsingPOSTResponse] Returns a {Seahorse::Client::Response response} object which responds to the following methods:
    #
    #   * {Types::sendEmailNotificationUsingPOSTResponse#email_notification_resource #email_notification_resource} => Types::EmailNotificationResource
    #
    # @example Request syntax with placeholder values
    #
    #   resp = client.send_email_notification_using_post({
    #     email_notification_resource: { # required
    #       create_time: Time.now,
    #       email_addresses: ["__string"],
    #       from_address: "__string",
    #       html_body: "__string",
    #       id: "__string",
    #       invalid_email_addresses: ["__string"],
    #       last_modified_time: Time.now,
    #       metadata: {
    #       },
    #       notification_system: {
    #         create_time: Time.now,
    #         description: "__string",
    #         id: "__string",
    #         last_modified_time: Time.now,
    #         name: "__string",
    #         reference_id: "__string",
    #       },
    #       reference_id: "__string",
    #       subject: "__string",
    #       text_body: "__string",
    #     },
    #     system_id: "__string", # required
    #   })
    #
    # @example Response structure
    #
    #   resp.email_notification_resource.create_time #=> Time
    #   resp.email_notification_resource.email_addresses #=> Array
    #   resp.email_notification_resource.email_addresses[0] #=> String
    #   resp.email_notification_resource.from_address #=> String
    #   resp.email_notification_resource.html_body #=> String
    #   resp.email_notification_resource.id #=> String
    #   resp.email_notification_resource.invalid_email_addresses #=> Array
    #   resp.email_notification_resource.invalid_email_addresses[0] #=> String
    #   resp.email_notification_resource.last_modified_time #=> Time
    #   resp.email_notification_resource.notification_system.create_time #=> Time
    #   resp.email_notification_resource.notification_system.description #=> String
    #   resp.email_notification_resource.notification_system.id #=> String
    #   resp.email_notification_resource.notification_system.last_modified_time #=> Time
    #   resp.email_notification_resource.notification_system.name #=> String
    #   resp.email_notification_resource.notification_system.reference_id #=> String
    #   resp.email_notification_resource.reference_id #=> String
    #   resp.email_notification_resource.subject #=> String
    #   resp.email_notification_resource.text_body #=> String
    #
    # @overload send_email_notification_using_post(params = {})
    # @param [Hash] params ({})
    def send_email_notification_using_post(params = {}, options = {})
      req = build_request(:send_email_notification_using_post, params)
      req.send_request(options)
    end

    # @option params [required, Types::SmsNotificationResource] :sms_notification_resource
    #
    # @option params [required, String] :system_id
    #
    # @return [Types::sendSmsNotificationUsingPOSTResponse] Returns a {Seahorse::Client::Response response} object which responds to the following methods:
    #
    #   * {Types::sendSmsNotificationUsingPOSTResponse#sms_notification_resource #sms_notification_resource} => Types::SmsNotificationResource
    #
    # @example Request syntax with placeholder values
    #
    #   resp = client.send_sms_notification_using_post({
    #     sms_notification_resource: { # required
    #       create_time: Time.now,
    #       id: "__string",
    #       invalid_sms_numbers: ["__string"],
    #       last_modified_time: Time.now,
    #       message: "__string",
    #       metadata: {
    #       },
    #       notification_system: {
    #         create_time: Time.now,
    #         description: "__string",
    #         id: "__string",
    #         last_modified_time: Time.now,
    #         name: "__string",
    #         reference_id: "__string",
    #       },
    #       reference_id: "__string",
    #       sms_numbers: ["__string"],
    #     },
    #     system_id: "__string", # required
    #   })
    #
    # @example Response structure
    #
    #   resp.sms_notification_resource.create_time #=> Time
    #   resp.sms_notification_resource.id #=> String
    #   resp.sms_notification_resource.invalid_sms_numbers #=> Array
    #   resp.sms_notification_resource.invalid_sms_numbers[0] #=> String
    #   resp.sms_notification_resource.last_modified_time #=> Time
    #   resp.sms_notification_resource.message #=> String
    #   resp.sms_notification_resource.notification_system.create_time #=> Time
    #   resp.sms_notification_resource.notification_system.description #=> String
    #   resp.sms_notification_resource.notification_system.id #=> String
    #   resp.sms_notification_resource.notification_system.last_modified_time #=> Time
    #   resp.sms_notification_resource.notification_system.name #=> String
    #   resp.sms_notification_resource.notification_system.reference_id #=> String
    #   resp.sms_notification_resource.reference_id #=> String
    #   resp.sms_notification_resource.sms_numbers #=> Array
    #   resp.sms_notification_resource.sms_numbers[0] #=> String
    #
    # @overload send_sms_notification_using_post(params = {})
    # @param [Hash] params ({})
    def send_sms_notification_using_post(params = {}, options = {})
      req = build_request(:send_sms_notification_using_post, params)
      req.send_request(options)
    end

    # @option params [required, Types::NotificationSystemResource] :notification_system_resource
    #
    # @option params [required, String] :system_id
    #
    # @return [Types::updateNotificationSystemUsingPUTResponse] Returns a {Seahorse::Client::Response response} object which responds to the following methods:
    #
    #   * {Types::updateNotificationSystemUsingPUTResponse#notification_system_resource #notification_system_resource} => Types::NotificationSystemResource
    #
    # @example Request syntax with placeholder values
    #
    #   resp = client.update_notification_system_using_put({
    #     notification_system_resource: { # required
    #       create_time: Time.now,
    #       description: "__string",
    #       id: "__string",
    #       last_modified_time: Time.now,
    #       name: "__string",
    #       reference_id: "__string",
    #     },
    #     system_id: "__string", # required
    #   })
    #
    # @example Response structure
    #
    #   resp.notification_system_resource.create_time #=> Time
    #   resp.notification_system_resource.description #=> String
    #   resp.notification_system_resource.id #=> String
    #   resp.notification_system_resource.last_modified_time #=> Time
    #   resp.notification_system_resource.name #=> String
    #   resp.notification_system_resource.reference_id #=> String
    #
    # @overload update_notification_system_using_put(params = {})
    # @param [Hash] params ({})
    def update_notification_system_using_put(params = {}, options = {})
      req = build_request(:update_notification_system_using_put, params)
      req.send_request(options)
    end

    # @!endgroup

    # @param params ({})
    # @api private
    def build_request(operation_name, params = {})
      handlers = @handlers.for(operation_name)
      authorizer = nil
      if config.api.operation(operation_name).authorizer
        authorizer_name = config.api.operation(operation_name).authorizer
        config.api.authorizers.each do |_, auth|
          authorizer = auth if auth.name == authorizer_name
        end
      end
      context = Seahorse::Client::RequestContext.new(
        operation_name: operation_name,
        operation: config.api.operation(operation_name),
        authorizer: authorizer,
        client: self,
        params: params,
        config: config)
      context[:gem_name] = 'proficient-notifications'
      context[:gem_version] = '1.0'
      Seahorse::Client::Request.new(handlers, context)
    end

    # @api private
    # @deprecated
    def waiter_names
      []
    end

    class << self

      # @api private
      attr_reader :identifier

      # @api private
      def errors_module
        Errors
      end

    end
  end
end
