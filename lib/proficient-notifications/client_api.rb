module Notifications
  # @api private
  module ClientApi

    include Seahorse::Model

    ArrayOfNotificationSystemResource = Shapes::ListShape.new(name: 'ArrayOfNotificationSystemResource')
    EmailNotificationResource = Shapes::StructureShape.new(name: 'EmailNotificationResource')
    ForbiddenException = Shapes::StructureShape.new(name: 'ForbiddenException')
    Metadata = Shapes::StructureShape.new(name: 'Metadata')
    NotFoundException = Shapes::StructureShape.new(name: 'NotFoundException')
    NotificationSystemResource = Shapes::StructureShape.new(name: 'NotificationSystemResource')
    SmsNotificationResource = Shapes::StructureShape.new(name: 'SmsNotificationResource')
    UnauthorizedException = Shapes::StructureShape.new(name: 'UnauthorizedException')
    boolean = Shapes::BooleanShape.new(name: 'boolean')
    double = Shapes::FloatShape.new(name: 'double')
    integer = Shapes::IntegerShape.new(name: 'integer')
    listOf__string = Shapes::ListShape.new(name: 'listOf__string')
    long = Shapes::IntegerShape.new(name: 'long')
    string = Shapes::StringShape.new(name: 'string')
    timestampIso8601 = Shapes::TimestampShape.new(name: 'timestampIso8601', timestampFormat: "iso8601")
    timestampUnix = Shapes::TimestampShape.new(name: 'timestampUnix', timestampFormat: "iso8601")
    CreateNotificationSystemUsingPOSTRequest = Shapes::StructureShape.new(name: 'CreateNotificationSystemUsingPOSTRequest')
    CreateNotificationSystemUsingPOSTResponse = Shapes::StructureShape.new(name: 'CreateNotificationSystemUsingPOSTResponse')
    DeleteNotificationSystemUsingDELETERequest = Shapes::StructureShape.new(name: 'DeleteNotificationSystemUsingDELETERequest')
    GetNotificationSystemUsingGETRequest = Shapes::StructureShape.new(name: 'GetNotificationSystemUsingGETRequest')
    GetNotificationSystemUsingGETResponse = Shapes::StructureShape.new(name: 'GetNotificationSystemUsingGETResponse')
    SearchNotificationSystemsUsingGETRequest = Shapes::StructureShape.new(name: 'SearchNotificationSystemsUsingGETRequest')
    SearchNotificationSystemsUsingGETResponse = Shapes::StructureShape.new(name: 'SearchNotificationSystemsUsingGETResponse')
    SendEmailNotificationUsingPOSTRequest = Shapes::StructureShape.new(name: 'SendEmailNotificationUsingPOSTRequest')
    SendEmailNotificationUsingPOSTResponse = Shapes::StructureShape.new(name: 'SendEmailNotificationUsingPOSTResponse')
    SendSmsNotificationUsingPOSTRequest = Shapes::StructureShape.new(name: 'SendSmsNotificationUsingPOSTRequest')
    SendSmsNotificationUsingPOSTResponse = Shapes::StructureShape.new(name: 'SendSmsNotificationUsingPOSTResponse')
    UpdateNotificationSystemUsingPUTRequest = Shapes::StructureShape.new(name: 'UpdateNotificationSystemUsingPUTRequest')
    UpdateNotificationSystemUsingPUTResponse = Shapes::StructureShape.new(name: 'UpdateNotificationSystemUsingPUTResponse')

    ArrayOfNotificationSystemResource.member = Shapes::ShapeRef.new(shape: NotificationSystemResource)

    EmailNotificationResource.add_member(:create_time, Shapes::ShapeRef.new(shape: timestampIso8601, location_name: "createTime"))
    EmailNotificationResource.add_member(:email_addresses, Shapes::ShapeRef.new(shape: listOf__string, location_name: "emailAddresses"))
    EmailNotificationResource.add_member(:from_address, Shapes::ShapeRef.new(shape: string, location_name: "fromAddress"))
    EmailNotificationResource.add_member(:html_body, Shapes::ShapeRef.new(shape: string, location_name: "htmlBody"))
    EmailNotificationResource.add_member(:id, Shapes::ShapeRef.new(shape: string, location_name: "id"))
    EmailNotificationResource.add_member(:invalid_email_addresses, Shapes::ShapeRef.new(shape: listOf__string, location_name: "invalidEmailAddresses"))
    EmailNotificationResource.add_member(:last_modified_time, Shapes::ShapeRef.new(shape: timestampIso8601, location_name: "lastModifiedTime"))
    EmailNotificationResource.add_member(:metadata, Shapes::ShapeRef.new(shape: Metadata, location_name: "metadata"))
    EmailNotificationResource.add_member(:notification_system, Shapes::ShapeRef.new(shape: NotificationSystemResource, location_name: "notificationSystem"))
    EmailNotificationResource.add_member(:reference_id, Shapes::ShapeRef.new(shape: string, location_name: "referenceId"))
    EmailNotificationResource.add_member(:subject, Shapes::ShapeRef.new(shape: string, location_name: "subject"))
    EmailNotificationResource.add_member(:text_body, Shapes::ShapeRef.new(shape: string, location_name: "textBody"))
    EmailNotificationResource.struct_class = Types::EmailNotificationResource

    Metadata.struct_class = Types::Metadata

    NotificationSystemResource.add_member(:create_time, Shapes::ShapeRef.new(shape: timestampIso8601, location_name: "createTime"))
    NotificationSystemResource.add_member(:description, Shapes::ShapeRef.new(shape: string, location_name: "description"))
    NotificationSystemResource.add_member(:id, Shapes::ShapeRef.new(shape: string, location_name: "id"))
    NotificationSystemResource.add_member(:last_modified_time, Shapes::ShapeRef.new(shape: timestampIso8601, location_name: "lastModifiedTime"))
    NotificationSystemResource.add_member(:name, Shapes::ShapeRef.new(shape: string, location_name: "name"))
    NotificationSystemResource.add_member(:reference_id, Shapes::ShapeRef.new(shape: string, location_name: "referenceId"))
    NotificationSystemResource.struct_class = Types::NotificationSystemResource

    SmsNotificationResource.add_member(:create_time, Shapes::ShapeRef.new(shape: timestampIso8601, location_name: "createTime"))
    SmsNotificationResource.add_member(:id, Shapes::ShapeRef.new(shape: string, location_name: "id"))
    SmsNotificationResource.add_member(:invalid_sms_numbers, Shapes::ShapeRef.new(shape: listOf__string, location_name: "invalidSmsNumbers"))
    SmsNotificationResource.add_member(:last_modified_time, Shapes::ShapeRef.new(shape: timestampIso8601, location_name: "lastModifiedTime"))
    SmsNotificationResource.add_member(:message, Shapes::ShapeRef.new(shape: string, location_name: "message"))
    SmsNotificationResource.add_member(:metadata, Shapes::ShapeRef.new(shape: Metadata, location_name: "metadata"))
    SmsNotificationResource.add_member(:notification_system, Shapes::ShapeRef.new(shape: NotificationSystemResource, location_name: "notificationSystem"))
    SmsNotificationResource.add_member(:reference_id, Shapes::ShapeRef.new(shape: string, location_name: "referenceId"))
    SmsNotificationResource.add_member(:sms_numbers, Shapes::ShapeRef.new(shape: listOf__string, location_name: "smsNumbers"))
    SmsNotificationResource.struct_class = Types::SmsNotificationResource

    listOf__string.member = Shapes::ShapeRef.new(shape: string)

    CreateNotificationSystemUsingPOSTRequest.add_member(:notification_system_resource, Shapes::ShapeRef.new(shape: NotificationSystemResource, required: true, location_name: "NotificationSystemResource"))
    CreateNotificationSystemUsingPOSTRequest.struct_class = Types::CreateNotificationSystemUsingPOSTRequest
    CreateNotificationSystemUsingPOSTRequest[:payload] = :notification_system_resource
    CreateNotificationSystemUsingPOSTRequest[:payload_member] = CreateNotificationSystemUsingPOSTRequest.member(:notification_system_resource)

    CreateNotificationSystemUsingPOSTResponse.add_member(:notification_system_resource, Shapes::ShapeRef.new(shape: NotificationSystemResource, required: true, location_name: "NotificationSystemResource"))
    CreateNotificationSystemUsingPOSTResponse.struct_class = Types::CreateNotificationSystemUsingPOSTResponse
    CreateNotificationSystemUsingPOSTResponse[:payload] = :notification_system_resource
    CreateNotificationSystemUsingPOSTResponse[:payload_member] = CreateNotificationSystemUsingPOSTResponse.member(:notification_system_resource)

    DeleteNotificationSystemUsingDELETERequest.add_member(:system_id, Shapes::ShapeRef.new(shape: string, required: true, location: "uri", location_name: "systemId"))
    DeleteNotificationSystemUsingDELETERequest.struct_class = Types::DeleteNotificationSystemUsingDELETERequest

    GetNotificationSystemUsingGETRequest.add_member(:system_id, Shapes::ShapeRef.new(shape: string, required: true, location: "uri", location_name: "systemId"))
    GetNotificationSystemUsingGETRequest.struct_class = Types::GetNotificationSystemUsingGETRequest

    GetNotificationSystemUsingGETResponse.add_member(:notification_system_resource, Shapes::ShapeRef.new(shape: NotificationSystemResource, required: true, location_name: "NotificationSystemResource"))
    GetNotificationSystemUsingGETResponse.struct_class = Types::GetNotificationSystemUsingGETResponse
    GetNotificationSystemUsingGETResponse[:payload] = :notification_system_resource
    GetNotificationSystemUsingGETResponse[:payload_member] = GetNotificationSystemUsingGETResponse.member(:notification_system_resource)

    SearchNotificationSystemsUsingGETRequest.add_member(:offset, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "offset"))
    SearchNotificationSystemsUsingGETRequest.add_member(:page_number, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "pageNumber"))
    SearchNotificationSystemsUsingGETRequest.add_member(:page_size, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "pageSize"))
    SearchNotificationSystemsUsingGETRequest.add_member(:paged, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "paged"))
    SearchNotificationSystemsUsingGETRequest.add_member(:search_term, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "searchTerm"))
    SearchNotificationSystemsUsingGETRequest.add_member(:sort_sorted, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "sort.sorted"))
    SearchNotificationSystemsUsingGETRequest.add_member(:sort_unsorted, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "sort.unsorted"))
    SearchNotificationSystemsUsingGETRequest.add_member(:unpaged, Shapes::ShapeRef.new(shape: string, location: "querystring", location_name: "unpaged"))
    SearchNotificationSystemsUsingGETRequest.struct_class = Types::SearchNotificationSystemsUsingGETRequest

    SearchNotificationSystemsUsingGETResponse.add_member(:array_of_notification_system_resource, Shapes::ShapeRef.new(shape: ArrayOfNotificationSystemResource, required: true, location_name: "ArrayOfNotificationSystemResource"))
    SearchNotificationSystemsUsingGETResponse.struct_class = Types::SearchNotificationSystemsUsingGETResponse
    SearchNotificationSystemsUsingGETResponse[:payload] = :array_of_notification_system_resource
    SearchNotificationSystemsUsingGETResponse[:payload_member] = SearchNotificationSystemsUsingGETResponse.member(:array_of_notification_system_resource)

    SendEmailNotificationUsingPOSTRequest.add_member(:email_notification_resource, Shapes::ShapeRef.new(shape: EmailNotificationResource, required: true, location_name: "EmailNotificationResource"))
    SendEmailNotificationUsingPOSTRequest.add_member(:system_id, Shapes::ShapeRef.new(shape: string, required: true, location: "uri", location_name: "systemId"))
    SendEmailNotificationUsingPOSTRequest.struct_class = Types::SendEmailNotificationUsingPOSTRequest
    SendEmailNotificationUsingPOSTRequest[:payload] = :email_notification_resource
    SendEmailNotificationUsingPOSTRequest[:payload_member] = SendEmailNotificationUsingPOSTRequest.member(:email_notification_resource)

    SendEmailNotificationUsingPOSTResponse.add_member(:email_notification_resource, Shapes::ShapeRef.new(shape: EmailNotificationResource, required: true, location_name: "EmailNotificationResource"))
    SendEmailNotificationUsingPOSTResponse.struct_class = Types::SendEmailNotificationUsingPOSTResponse
    SendEmailNotificationUsingPOSTResponse[:payload] = :email_notification_resource
    SendEmailNotificationUsingPOSTResponse[:payload_member] = SendEmailNotificationUsingPOSTResponse.member(:email_notification_resource)

    SendSmsNotificationUsingPOSTRequest.add_member(:sms_notification_resource, Shapes::ShapeRef.new(shape: SmsNotificationResource, required: true, location_name: "SmsNotificationResource"))
    SendSmsNotificationUsingPOSTRequest.add_member(:system_id, Shapes::ShapeRef.new(shape: string, required: true, location: "uri", location_name: "systemId"))
    SendSmsNotificationUsingPOSTRequest.struct_class = Types::SendSmsNotificationUsingPOSTRequest
    SendSmsNotificationUsingPOSTRequest[:payload] = :sms_notification_resource
    SendSmsNotificationUsingPOSTRequest[:payload_member] = SendSmsNotificationUsingPOSTRequest.member(:sms_notification_resource)

    SendSmsNotificationUsingPOSTResponse.add_member(:sms_notification_resource, Shapes::ShapeRef.new(shape: SmsNotificationResource, required: true, location_name: "SmsNotificationResource"))
    SendSmsNotificationUsingPOSTResponse.struct_class = Types::SendSmsNotificationUsingPOSTResponse
    SendSmsNotificationUsingPOSTResponse[:payload] = :sms_notification_resource
    SendSmsNotificationUsingPOSTResponse[:payload_member] = SendSmsNotificationUsingPOSTResponse.member(:sms_notification_resource)

    UpdateNotificationSystemUsingPUTRequest.add_member(:notification_system_resource, Shapes::ShapeRef.new(shape: NotificationSystemResource, required: true, location_name: "NotificationSystemResource"))
    UpdateNotificationSystemUsingPUTRequest.add_member(:system_id, Shapes::ShapeRef.new(shape: string, required: true, location: "uri", location_name: "systemId"))
    UpdateNotificationSystemUsingPUTRequest.struct_class = Types::UpdateNotificationSystemUsingPUTRequest
    UpdateNotificationSystemUsingPUTRequest[:payload] = :notification_system_resource
    UpdateNotificationSystemUsingPUTRequest[:payload_member] = UpdateNotificationSystemUsingPUTRequest.member(:notification_system_resource)

    UpdateNotificationSystemUsingPUTResponse.add_member(:notification_system_resource, Shapes::ShapeRef.new(shape: NotificationSystemResource, required: true, location_name: "NotificationSystemResource"))
    UpdateNotificationSystemUsingPUTResponse.struct_class = Types::UpdateNotificationSystemUsingPUTResponse
    UpdateNotificationSystemUsingPUTResponse[:payload] = :notification_system_resource
    UpdateNotificationSystemUsingPUTResponse[:payload_member] = UpdateNotificationSystemUsingPUTResponse.member(:notification_system_resource)


    # @api private
    API = Seahorse::Model::Api.new.tap do |api|

      api.version = "2020-06-05T21:04:48Z"

      api.metadata = {
        "apiVersion" => "2020-06-05T21:04:48Z",
        "endpointPrefix" => "7guj3fsapj",
        "protocol" => "api-gateway",
        "serviceFullName" => "Notification Management REST",
        "serviceId" => "Notification Management REST",
        "uid" => "7guj3fsapj-2020-06-05T21:04:48Z",
      }

      api.add_operation(:create_notification_system_using_post, Seahorse::Model::Operation.new.tap do |o|
        o.name = "createNotificationSystemUsingPOST"
        o.http_method = "POST"
        o.http_request_uri = "/notification-systems"
        o['authtype'] = "none"
        o.require_apikey = true
        o.input = Shapes::ShapeRef.new(shape: CreateNotificationSystemUsingPOSTRequest)
        o.output = Shapes::ShapeRef.new(shape: CreateNotificationSystemUsingPOSTResponse)
        o.errors << Shapes::ShapeRef.new(shape: NotFoundException)
        o.errors << Shapes::ShapeRef.new(shape: UnauthorizedException)
        o.errors << Shapes::ShapeRef.new(shape: ForbiddenException)
      end)

      api.add_operation(:delete_notification_system_using_delete, Seahorse::Model::Operation.new.tap do |o|
        o.name = "deleteNotificationSystemUsingDELETE"
        o.http_method = "DELETE"
        o.http_request_uri = "/notification-systems/{systemId}"
        o['authtype'] = "none"
        o.require_apikey = true
        o.input = Shapes::ShapeRef.new(shape: DeleteNotificationSystemUsingDELETERequest)
        o.output = Shapes::ShapeRef.new(shape: Shapes::StructureShape.new(struct_class: Aws::EmptyStructure))
        o.errors << Shapes::ShapeRef.new(shape: UnauthorizedException)
        o.errors << Shapes::ShapeRef.new(shape: ForbiddenException)
      end)

      api.add_operation(:get_notification_system_using_get, Seahorse::Model::Operation.new.tap do |o|
        o.name = "getNotificationSystemUsingGET"
        o.http_method = "GET"
        o.http_request_uri = "/notification-systems/{systemId}"
        o['authtype'] = "none"
        o.require_apikey = true
        o.input = Shapes::ShapeRef.new(shape: GetNotificationSystemUsingGETRequest)
        o.output = Shapes::ShapeRef.new(shape: GetNotificationSystemUsingGETResponse)
        o.errors << Shapes::ShapeRef.new(shape: NotFoundException)
        o.errors << Shapes::ShapeRef.new(shape: UnauthorizedException)
        o.errors << Shapes::ShapeRef.new(shape: ForbiddenException)
      end)

      api.add_operation(:search_notification_systems_using_get, Seahorse::Model::Operation.new.tap do |o|
        o.name = "searchNotificationSystemsUsingGET"
        o.http_method = "GET"
        o.http_request_uri = "/notification-systems"
        o['authtype'] = "none"
        o.require_apikey = true
        o.input = Shapes::ShapeRef.new(shape: SearchNotificationSystemsUsingGETRequest)
        o.output = Shapes::ShapeRef.new(shape: SearchNotificationSystemsUsingGETResponse)
        o.errors << Shapes::ShapeRef.new(shape: NotFoundException)
        o.errors << Shapes::ShapeRef.new(shape: UnauthorizedException)
        o.errors << Shapes::ShapeRef.new(shape: ForbiddenException)
      end)

      api.add_operation(:send_email_notification_using_post, Seahorse::Model::Operation.new.tap do |o|
        o.name = "sendEmailNotificationUsingPOST"
        o.http_method = "POST"
        o.http_request_uri = "/notification-systems/{systemId}/mass-send-email"
        o['authtype'] = "none"
        o.require_apikey = true
        o.input = Shapes::ShapeRef.new(shape: SendEmailNotificationUsingPOSTRequest)
        o.output = Shapes::ShapeRef.new(shape: SendEmailNotificationUsingPOSTResponse)
        o.errors << Shapes::ShapeRef.new(shape: NotFoundException)
        o.errors << Shapes::ShapeRef.new(shape: UnauthorizedException)
        o.errors << Shapes::ShapeRef.new(shape: ForbiddenException)
      end)

      api.add_operation(:send_sms_notification_using_post, Seahorse::Model::Operation.new.tap do |o|
        o.name = "sendSmsNotificationUsingPOST"
        o.http_method = "POST"
        o.http_request_uri = "/notification-systems/{systemId}/mass-send-sms"
        o['authtype'] = "none"
        o.require_apikey = true
        o.input = Shapes::ShapeRef.new(shape: SendSmsNotificationUsingPOSTRequest)
        o.output = Shapes::ShapeRef.new(shape: SendSmsNotificationUsingPOSTResponse)
        o.errors << Shapes::ShapeRef.new(shape: NotFoundException)
        o.errors << Shapes::ShapeRef.new(shape: UnauthorizedException)
        o.errors << Shapes::ShapeRef.new(shape: ForbiddenException)
      end)

      api.add_operation(:update_notification_system_using_put, Seahorse::Model::Operation.new.tap do |o|
        o.name = "updateNotificationSystemUsingPUT"
        o.http_method = "PUT"
        o.http_request_uri = "/notification-systems/{systemId}"
        o['authtype'] = "none"
        o.require_apikey = true
        o.input = Shapes::ShapeRef.new(shape: UpdateNotificationSystemUsingPUTRequest)
        o.output = Shapes::ShapeRef.new(shape: UpdateNotificationSystemUsingPUTResponse)
        o.errors << Shapes::ShapeRef.new(shape: NotFoundException)
        o.errors << Shapes::ShapeRef.new(shape: UnauthorizedException)
        o.errors << Shapes::ShapeRef.new(shape: ForbiddenException)
      end)
    end

  end
end
