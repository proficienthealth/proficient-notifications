module Notifications
  module Plugins

    class APIGEndpoint < Seahorse::Client::Plugin

      def after_initialize(client)
        if client.config.endpoint.nil?
          client.config.endpoint = 'https://7guj3fsapj.execute-api.us-east-1.amazonaws.com'
        end
      end

    end
  end
end
