module Notifications
  module Types

    # @note When making an API call, you may pass EmailNotificationResource
    #   data as a hash:
    #
    #       {
    #         create_time: Time.now,
    #         email_addresses: ["__string"],
    #         from_address: "__string",
    #         html_body: "__string",
    #         id: "__string",
    #         invalid_email_addresses: ["__string"],
    #         last_modified_time: Time.now,
    #         metadata: {
    #         },
    #         notification_system: {
    #           create_time: Time.now,
    #           description: "__string",
    #           id: "__string",
    #           last_modified_time: Time.now,
    #           name: "__string",
    #           reference_id: "__string",
    #         },
    #         reference_id: "__string",
    #         subject: "__string",
    #         text_body: "__string",
    #       }
    #
    # @!attribute [rw] create_time
    #   @return [Time]
    #
    # @!attribute [rw] email_addresses
    #   @return [Array<String>]
    #
    # @!attribute [rw] from_address
    #   @return [String]
    #
    # @!attribute [rw] html_body
    #   @return [String]
    #
    # @!attribute [rw] id
    #   @return [String]
    #
    # @!attribute [rw] invalid_email_addresses
    #   @return [Array<String>]
    #
    # @!attribute [rw] last_modified_time
    #   @return [Time]
    #
    # @!attribute [rw] metadata
    #   @return [Types::Metadata]
    #
    # @!attribute [rw] notification_system
    #   @return [Types::NotificationSystemResource]
    #
    # @!attribute [rw] reference_id
    #   @return [String]
    #
    # @!attribute [rw] subject
    #   @return [String]
    #
    # @!attribute [rw] text_body
    #   @return [String]
    #
    class EmailNotificationResource < Struct.new(
      :create_time,
      :email_addresses,
      :from_address,
      :html_body,
      :id,
      :invalid_email_addresses,
      :last_modified_time,
      :metadata,
      :notification_system,
      :reference_id,
      :subject,
      :text_body)
      include Aws::Structure
    end

    # @api private
    #
    class Metadata < Aws::EmptyStructure; end

    # @note When making an API call, you may pass NotificationSystemResource
    #   data as a hash:
    #
    #       {
    #         create_time: Time.now,
    #         description: "__string",
    #         id: "__string",
    #         last_modified_time: Time.now,
    #         name: "__string",
    #         reference_id: "__string",
    #       }
    #
    # @!attribute [rw] create_time
    #   @return [Time]
    #
    # @!attribute [rw] description
    #   @return [String]
    #
    # @!attribute [rw] id
    #   @return [String]
    #
    # @!attribute [rw] last_modified_time
    #   @return [Time]
    #
    # @!attribute [rw] name
    #   @return [String]
    #
    # @!attribute [rw] reference_id
    #   @return [String]
    #
    class NotificationSystemResource < Struct.new(
      :create_time,
      :description,
      :id,
      :last_modified_time,
      :name,
      :reference_id)
      include Aws::Structure
    end

    # @note When making an API call, you may pass SmsNotificationResource
    #   data as a hash:
    #
    #       {
    #         create_time: Time.now,
    #         id: "__string",
    #         invalid_sms_numbers: ["__string"],
    #         last_modified_time: Time.now,
    #         message: "__string",
    #         metadata: {
    #         },
    #         notification_system: {
    #           create_time: Time.now,
    #           description: "__string",
    #           id: "__string",
    #           last_modified_time: Time.now,
    #           name: "__string",
    #           reference_id: "__string",
    #         },
    #         reference_id: "__string",
    #         sms_numbers: ["__string"],
    #       }
    #
    # @!attribute [rw] create_time
    #   @return [Time]
    #
    # @!attribute [rw] id
    #   @return [String]
    #
    # @!attribute [rw] invalid_sms_numbers
    #   @return [Array<String>]
    #
    # @!attribute [rw] last_modified_time
    #   @return [Time]
    #
    # @!attribute [rw] message
    #   @return [String]
    #
    # @!attribute [rw] metadata
    #   @return [Types::Metadata]
    #
    # @!attribute [rw] notification_system
    #   @return [Types::NotificationSystemResource]
    #
    # @!attribute [rw] reference_id
    #   @return [String]
    #
    # @!attribute [rw] sms_numbers
    #   @return [Array<String>]
    #
    class SmsNotificationResource < Struct.new(
      :create_time,
      :id,
      :invalid_sms_numbers,
      :last_modified_time,
      :message,
      :metadata,
      :notification_system,
      :reference_id,
      :sms_numbers)
      include Aws::Structure
    end

    # @!attribute [rw] notification_system_resource
    #   @return [Types::NotificationSystemResource]
    #
    class CreateNotificationSystemUsingPOSTRequest < Struct.new(
      :notification_system_resource)
      include Aws::Structure
    end

    # @!attribute [rw] notification_system_resource
    #   @return [Types::NotificationSystemResource]
    #
    class CreateNotificationSystemUsingPOSTResponse < Struct.new(
      :notification_system_resource)
      include Aws::Structure
    end

    # @!attribute [rw] system_id
    #   @return [String]
    #
    class DeleteNotificationSystemUsingDELETERequest < Struct.new(
      :system_id)
      include Aws::Structure
    end

    # @!attribute [rw] system_id
    #   @return [String]
    #
    class GetNotificationSystemUsingGETRequest < Struct.new(
      :system_id)
      include Aws::Structure
    end

    # @!attribute [rw] notification_system_resource
    #   @return [Types::NotificationSystemResource]
    #
    class GetNotificationSystemUsingGETResponse < Struct.new(
      :notification_system_resource)
      include Aws::Structure
    end

    # @!attribute [rw] offset
    #   @return [String]
    #
    # @!attribute [rw] page_number
    #   @return [String]
    #
    # @!attribute [rw] page_size
    #   @return [String]
    #
    # @!attribute [rw] paged
    #   @return [String]
    #
    # @!attribute [rw] search_term
    #   @return [String]
    #
    # @!attribute [rw] sort_sorted
    #   @return [String]
    #
    # @!attribute [rw] sort_unsorted
    #   @return [String]
    #
    # @!attribute [rw] unpaged
    #   @return [String]
    #
    class SearchNotificationSystemsUsingGETRequest < Struct.new(
      :offset,
      :page_number,
      :page_size,
      :paged,
      :search_term,
      :sort_sorted,
      :sort_unsorted,
      :unpaged)
      include Aws::Structure
    end

    # @!attribute [rw] array_of_notification_system_resource
    #   @return [Array<Types::NotificationSystemResource>]
    #
    class SearchNotificationSystemsUsingGETResponse < Struct.new(
      :array_of_notification_system_resource)
      include Aws::Structure
    end

    # @!attribute [rw] email_notification_resource
    #   @return [Types::EmailNotificationResource]
    #
    # @!attribute [rw] system_id
    #   @return [String]
    #
    class SendEmailNotificationUsingPOSTRequest < Struct.new(
      :email_notification_resource,
      :system_id)
      include Aws::Structure
    end

    # @!attribute [rw] email_notification_resource
    #   @return [Types::EmailNotificationResource]
    #
    class SendEmailNotificationUsingPOSTResponse < Struct.new(
      :email_notification_resource)
      include Aws::Structure
    end

    # @!attribute [rw] sms_notification_resource
    #   @return [Types::SmsNotificationResource]
    #
    # @!attribute [rw] system_id
    #   @return [String]
    #
    class SendSmsNotificationUsingPOSTRequest < Struct.new(
      :sms_notification_resource,
      :system_id)
      include Aws::Structure
    end

    # @!attribute [rw] sms_notification_resource
    #   @return [Types::SmsNotificationResource]
    #
    class SendSmsNotificationUsingPOSTResponse < Struct.new(
      :sms_notification_resource)
      include Aws::Structure
    end

    # @!attribute [rw] notification_system_resource
    #   @return [Types::NotificationSystemResource]
    #
    # @!attribute [rw] system_id
    #   @return [String]
    #
    class UpdateNotificationSystemUsingPUTRequest < Struct.new(
      :notification_system_resource,
      :system_id)
      include Aws::Structure
    end

    # @!attribute [rw] notification_system_resource
    #   @return [Types::NotificationSystemResource]
    #
    class UpdateNotificationSystemUsingPUTResponse < Struct.new(
      :notification_system_resource)
      include Aws::Structure
    end

  end
end
