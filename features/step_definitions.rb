Before("@notifications") do
  @service = Notifications::Resource.new
  @client = @service.client
end

After("@notifications") do
  # shared cleanup logic
end
